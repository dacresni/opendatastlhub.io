---
layout: default
---
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="javascript">
    function getStuff(url,id) {
            return $.getJSON(url, function(data) {
                $.each(data,function(i,repo){
                   console.log("index="+i+"item"+repo.name);
		   dt= $("<dt/>").append(repo.name);
		   dd = $("<dd/>",{'html':repo.description}).append("<a href=\'"+repo.html_url+"\'><i class=\"icon-circle-arrow-right\"></i></a>");
		   dt.appendTo(id);
		   dd.appendTo(id);
		   })

	    });
    }
    $.when(
	getStuff("https://api.github.com/orgs/opendatastl/members","#members") )
	getStuff("https://api.github.com/orgs/opendatastl/repos","#watched") )
    .then(function(){
    $(".span6").accordion({ header:"dt", collapsible:true, heightStyle:"content" }) ;
    })
</script>
